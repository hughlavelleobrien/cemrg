Built using [Jekyll templating engine](https://jekyllrb.com/)

To run locally:

- Clone the repository
- If ruby not installed
`sudo apt-get install ruby-dev`
- Install depts inside the repository
```
bundle install
bundle exec jekyll serve
```

The site should be accessible from localhost:4000


-----------------

Binaries for CEMRG app are not kept in git due to their large size
