---
layout: default
title: Arrhythmia
category: project
listImage: "/images/projects/arrhythmia/LA_491_1633.png"
description: Cardiac arrhythmias are abnormal heart rhythms, which affect more than 2 million people in the UK alone every year. Atrial fibrillation (AF) and ventricular tachycardia (VT) are two common cardiac arrhythmias. AF is a supraventricular tachyarrhythmia that is characterised by uncoordinated atrial activation with consequent deterioration of mechanical function. AF increases the risk of other cardiovascular diseases, stroke and death. During VT, abnormal electrical signals in the ventricles cause the heart to beat fast and in an uncoordinated manner. As a result of this arrhythmia, the heart is not able to pump enough blood to the body and lungs, which can lead to loss of consciousness and sudden death if left untreated.
path: projects/Arrhythmia
---

{% if jekyll.environment == "gitlab" %}
{% assign baseurl = "/cemrg" %}
{% else %}
{% assign baseurl = site.baseurl %}
{% endif %}

{% assign cesare = "Cesare Corrado" | url_encode %}
{% assign marina_m = "Marina Riabiz" | url_encode %}
{% assign gabriel = "Gabriel Balaban" | url_encode %}
{% assign caroline_c = "Caroline Costa" | url_encode %}
{% assign caroline_r = "Caroline Roney" | url_encode %}
{% assign fernando = "Fernando Campos" | url_encode %}

<div class='full'>
  <div class="row">
    <div class='large-12 columns'>
      <p>
        <img class="title-img" src={{"/images/projects/arrhythmia/titleimage.svg" | prepend: baseurl}} />
      </p>
      <h2> Overview </h2>
      <p>
        Cardiac arrhythmias are abnormal heart rhythms, which affect more than 2 million people in the UK alone every year. Atrial fibrillation (AF) and ventricular tachycardia (VT) are two common cardiac arrhythmias. AF is a supraventricular tachyarrhythmia that is characterised by uncoordinated atrial activation with consequent deterioration of mechanical function. AF increases the risk of other cardiovascular diseases, stroke and death. During VT, abnormal electrical signals in the ventricles cause the heart to beat fast and in an uncoordinated manner. As a result of this arrhythmia, the heart is not able to pump enough blood to the body and lungs, which can lead to loss of consciousness and sudden death if left untreated.
      </p>
      <p>
        Computer models are important for studying heart rhythm diseases because they provide a formal framework that combines our understanding of myocardial tissue physiology and physical constraints. However, building computational models tailored to a specific patient and using them in the clinical practice remains a technological challenge.
      </p>
      <p>
        Our group uses a combination of biophysical modelling, signal processing, image analysis and machine learning techniques to investigate the mechanisms underlying cardiac arrhythmias to inform patient selection, disease management and procedure planning. Specifically, we work closely with the clinical teams at St Thomas' hospital, as well as many collaborators, on the following research areas.
      </p>
    </div>
    <div class='large-12 columns'>
      <p>
        <img src={{"/images/projects/arrhythmia/atria_ventricles.svg" | prepend: baseurl}}>
        <i>Simulated arrhythmia: electrical activity in the top two chambers of the heart (atria), and in the bottom two chambers (ventricles).</i>
      </p>
    </div>
  </div>
  <div class="row">
    <div class='large-12 columns'>
      <h2> Research Areas </h2>
	    <h3> Arrhythmia Mechanisms </h3>		
	    <p>
	      Understanding the mechanisms underlying cardiac arrhythmia initiation and maintenance using computational modelling is a key area of our research. Specifically, we investigate the effects of <a href="https://www.frontiersin.org/articles/10.3389/fphys.2018.01832/abstract"> scar tissue distribution</a> on arrhythmia in nonischemic cardiomyopathy and atrial fibrillation; the effects of <a href="https://www.ncbi.nlm.nih.gov/pubmed/28964108">repolarisation gradients on reentry initiation and arrhythmia dynamics</a>; and the effects of cardiac structure on <a href="https://www.ncbi.nlm.nih.gov/pubmed/28964115"> defibrillation shock protocols</a>. 
        <br> <i> <a href={{gabriel | prepend: "/people/team#" | prepend: baseurl}}>Gabriel Balaban</a>,  <a href={{fernando | prepend: "/people/team#" | prepend: baseurl}}>Fernando Campos</a>,  <a href={{caroline_c | prepend: "/people/team#" | prepend: baseurl}}>Caroline Costa</a> </i>
	    </p>
	      
	    <h3> Signal Processing </h3>		
	    <p>
	      Cardiac arrhythmia electrical recordings are important for diagnosing and understanding the mechanisms of arrhythmias to determine treatment strategies. However, this assessment is challenging because arrhythmia recordings are typically noisy, low resolution and difficult to interpret. As such, one area of our research is in developing techniques for analysing atrial fibrillation recordings using <a href="https://www.ncbi.nlm.nih.gov/pubmed/29858382"> phase mapping</a>, and for calculating substrate properties such as <a href="https://www.ncbi.nlm.nih.gov/pubmed/30415767"> conduction anisotropy</a>.  
        <br> <i> <a href={{caroline_r | prepend: "/people/team#" | prepend: baseurl}}>Caroline Roney</a> </i>
	    </p>

	    <h3> Patient Specific Modelling </h3>
	    <p>
	      Patient-specific models are personalised to data from an individual patient, and may be used to predict arrhythmia properties and the <a href="https://www.ncbi.nlm.nih.gov/pubmed/30476055"> efficacy of different treatment approaches</a>.  
        These personalised models combine the specific anatomy and <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5900020/"> fibrosis distribution</a> obtained from imaging data, with the electrophysiological properties of the tissue substrate obtained from <a href="https://www.ncbi.nlm.nih.gov/pubmed/29753180"> electroanatomical measurements</a>.  
        <br> <i> <a href={{gabriel | prepend: "/people/team#" | prepend: baseurl}}>Gabriel Balaban</a>, <a href={{cesare | prepend: "/people/team#" | prepend: baseurl}}>Cesare Corrado</a>, <a href={{caroline_c | prepend: "/people/team#" | prepend: baseurl}}>Caroline Costa</a>, <a href={{caroline_r | prepend: "/people/team#" | prepend: baseurl}}>Caroline Roney</a> </i>
      </p>
	    <h3>Uncertainty Quantification</h3>
	    <p>
	      Clinical measurements are inherently noisy and typically are not available for the whole organ. Hence, patient-specific models are affected by this uncertainty, which impacts the predictive performance of the model.
	      One branch of our research focuses on the quantification of model uncertainty in personalised computational models, both in term of the parameter inference and the predictive outcome. 
        <br>
        <i><a href={{cesare | prepend: "/people/team#" | prepend: baseurl}}>Cesare Corrado</a>, <a href={{marina_m | prepend: "/people/team#" | prepend: baseurl}}>Marina Riabiz</a> </i>
	    </p>
    </div>
  </div>
</div>
